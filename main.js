var renderer, camera, scene, imgdata;
function main()
{
    var points;
    // set the scene size
    var WIDTH = window.innerWidth,
    HEIGHT = window.innerHeight;

    // set some camera attributes
    var VIEW_ANGLE = 75,
    ASPECT = WIDTH / HEIGHT,
    NEAR = 0.1,
    FAR = 1000;

    renderer = new THREE.WebGLRenderer();
    camera = new THREE.PerspectiveCamera(VIEW_ANGLE, ASPECT, NEAR, FAR);
    scene = new THREE.Scene();
    camera.position.z = 300;

    renderer.setSize(WIDTH, HEIGHT);
    document.body.appendChild(renderer.domElement);
    
    function getImageData(image)
    {
        // console.log(image);
        var canvas = document.createElement("canvas");
        canvas.width = image.width;
        canvas.height = image.height;

        var ctx = canvas.getContext("2d");
        ctx.drawImage(image, 0, 0);

        imgdata = ctx.getImageData(0, 0, image.width, image.height);
        var pixels = imgdata.data;

        for (var i = 0, n = pixels.length; i < n; i += 4) {
            var grayscale = Math.floor((pixels[i] + pixels[i+1] + pixels[i+2]) / 3);
            pixels[i  ] = grayscale;        // red
            pixels[i+1] = grayscale;        // green
            pixels[i+2] = grayscale;        // blue
        }

        return imgdata;
    }

    function drawParticles()
    {
        var particles = new THREE.Geometry();
        var material = new THREE.PointsMaterial({color: 0x313742, size: 3});

        for(var y = 0; y < imgdata.height; y += 2)
        {
            for(var x = 0; x < imgdata.width; x += 2)
            {
                if (imgdata.data[(x * 4 + y * 4 * imgdata.width)] > 128) {
                
                    var vertex = new THREE.Vector3();
                    vertex.x = x - imgdata.width / 2; //Math.random() * 1000 - 500;
                    vertex.y = -y + imgdata.width / 2; //Math.random() * 1000 - 500;
                    vertex.z = 0; //-Math.random() * 500;
    
                    // vertex.destination = {
                    //     x: x - imgdata.width / 2,
                    //     y: -y + imgdata.height / 2,
                    //     z: 0
                    // };
    
                    vertex.speed = Math.random() / 200 + 0.015;
    
                    particles.vertices.push(vertex);
                }
            }
        }
        
        points = new THREE.Points( particles, material );
        scene.add(points);
    }

    var texture = THREE.ImageUtils.loadTexture("./img1.jpg", undefined, function(){
        imgdata = getImageData(texture.image);
        drawParticles();
    })

    var img;
    var loader = new THREE.TextureLoader();
    loader.load('./img1.jpg', function(text){
        img = getImageData(text.image);
    });
    console.log(img);

    function render()
    {
        requestAnimationFrame(render);
        // points.rotation.y -= 0.01;
        renderer.render(scene, camera);
    }

    render();
}