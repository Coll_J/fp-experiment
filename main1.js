var renderer, scene, camera, ww, wh, particles, vidtext, vidimgdata, context, cvs, video, geometry, material;
var flag = false, play = false;

ww = window.innerWidth,
wh = window.innerHeight;

var centerVector = new THREE.Vector3(0, 0, 0);
var previousTime = 0;

var getImageData = function(image) {

	var canvas = document.createElement("canvas");
	canvas.width = image.width;
    canvas.height = image.height;
    console.log("image " + image);

	var ctx = canvas.getContext("2d");
	ctx.drawImage(image, 0, 0);

	imgdata = ctx.getImageData(0, 0, image.width, image.height);
    var pixels = imgdata.data;

    for (var i = 0, n = pixels.length; i < n; i += 4) {
        var grayscale = Math.floor((pixels[i] + pixels[i+1] + pixels[i+2]) / 3);
        pixels[i  ] = grayscale;        // red
        pixels[i+1] = grayscale;        // green
        pixels[i+2] = grayscale;        // blue
        //pixels[i+3]              is alpha
    }

    return imgdata;
}

var drawTheMap = function() {

	geometry = new THREE.Geometry();
	material = new THREE.PointsMaterial({
		size: 1,
		color: 0x313742,
		sizeAttenuation: false
	});
	for (var y = 0, y2 = imagedata.height; y < y2; y += 2) {
		for (var x = 0, x2 = imagedata.width; x < x2; x += 2) {
			if (imagedata.data[(x * 4 + y * 4 * imagedata.width)] > 100) {

				var vertex = new THREE.Vector3();
				vertex.x = Math.random() * 1000 - 500;
				vertex.y = Math.random() * 1000 - 500;
				vertex.z = -Math.random() * 500;

				vertex.destination = {
					x: x - imagedata.width / 2,
					y: -y + imagedata.height / 2,
					z: 0
				};

				vertex.speed = Math.random() / 200 + 0.015;

                geometry.vertices.push(vertex);
                geometry.colors.push(0x00ffff);
                // console.log("geom color " + /geometry.colors);

			}
		}
	}
	particles = new THREE.Points(geometry, material);

	scene.add(particles);

	requestAnimationFrame(render);
};

var init = function() {
	var WIDTH = window.innerWidth,
    HEIGHT = window.innerHeight;

    // set some camera attributes
    var VIEW_ANGLE = 75,
    ASPECT = WIDTH / HEIGHT,
    NEAR = 0.1,
    FAR = 1000;

    renderer = new THREE.WebGLRenderer();
    camera = new THREE.PerspectiveCamera(VIEW_ANGLE, ASPECT, NEAR, FAR);
    scene = new THREE.Scene();
    camera.position.z = 300;

    renderer.setSize(WIDTH, HEIGHT);
    document.body.appendChild(renderer.domElement);

	texture = THREE.ImageUtils.loadTexture("./img1.jpg", undefined, function() {
        console.log("t " + texture.image);
		imagedata = getImageData(texture.image);
		drawTheMap();
    });
    
    //   window.addEventListener('resize', onResize, false);
    
};
var onResize = function(){
    ww = window.innerWidth;
	wh = window.innerHeight;
	renderer.setSize(ww, wh);
    camera.aspect = ww / wh;
    camera.updateProjectionMatrix();
};

var onClick = function()
{
    if(flag == false)
    {
        video = document.getElementById("video");
        // video.src = "./itzy.mp4";
        // video.load();
        // video.width = 480;
        // video.height = 204;
        // console.log(video);
        cvs = document.createElement("canvas");
        context = cvs.getContext('2d');
        context.drawImage(video, 0, 0, video.videoWidth / 2, video.videoHeight / 2);
        // dataurl = canvas.toDataURL();
        
        console.log(video.videoWidth);
        
        flag = true;
        
    }
    if(play)
    {
        play = false;
        video.stop()
    }
    else
    {
        play = true;
        video.play();
    }
}

var getFrameData = function()
{
    for (var i = 0, n = vidimgdata.data.length; i < n; i += 4) {
        var grayscale = Math.floor((vidimgdata.data[i] + vidimgdata.data[i+1] + vidimgdata.data[i+2]) / 3);
        vidimgdata.data[i  ] = grayscale;        // red
        vidimgdata.data[i+1] = grayscale;        // green
        vidimgdata.data[i+2] = grayscale;        // blue
        //pixels[i+3]              is alpha
    }
}
var updateParticles = function()
{
    getFrameData();
    geometry.vertices = [];
    for (var y = 0, y2 = vidimgdata.height; y < y2; y += 2) {
		for (var x = 0, x2 = vidimgdata.width; x < x2; x += 2) {
			if (vidimgdata.data[(x * 4 + y * 4 * vidimgdata.width)] > 80) {

				var vertex = new THREE.Vector3();
				vertex.x = x - vidimgdata.width / 2;// Math.random() * 1000 - 500;
				vertex.y = -y + vidimgdata.height / 2;//Math.random() * 1000 - 500;
				vertex.z = 0;//-Math.random() * 500;

				vertex.destination = {
					x: x - vidimgdata.width / 2,
					y: -y + vidimgdata.height / 2,
					z: 0
				};

				vertex.speed = Math.random() / 200 + 0.015;

                geometry.vertices.push(vertex);
                // console.log("geom color " + /geometry.colors);

			}
		}
	}
}

var render = function(a) {
    
    requestAnimationFrame(render);
    if(flag) 
    {
        context.drawImage(video, 0, 0, video.videoWidth / 2, video.videoHeight / 2);
        vidimgdata = context.getImageData(0, 0, video.videoWidth / 2, video.videoHeight / 2);
        updateParticles();
        // console.log(vidimgdata.data[0]);
        // console.log(vidimgdata.data[1]);
        // console.log(vidimgdata.data[2]);
        // console.log(vidimgdata.data[3]);

    }
    else
    {
        for (var i = 0, j = particles.geometry.vertices.length; i < j; i++) {
            var particle = particles.geometry.vertices[i];
            particle.x += (particle.destination.x - particle.x) * particle.speed;
            particle.y += (particle.destination.y - particle.y) * particle.speed;
            particle.z += (particle.destination.z - particle.z) * particle.speed;
        }
    }
    
    // if(flag) console.log(vidtext.image);

	// if(a-previousTime>100){
	// 	var index = Math.floor(Math.random()*particles.geometry.vertices.length);
	// 	var particle1 = particles.geometry.vertices[index];
	// 	var particle2 = particles.geometry.vertices[particles.geometry.vertices.length-index];
	// 	// TweenMax.to(particle, Math.random()*2+1,{x:particle2.x, y:particle2.y, ease:Power2.easeInOut});
	// 	// TweenMax.to(particle2, Math.random()*2+1,{x:particle1.x, y:particle1.y, ease:Power2.easeInOut});
	// 	previousTime = a;
	// }

    particles.geometry.verticesNeedUpdate = true;
    particles.rotation.y -= 0.01;
	renderer.render(scene, camera);
};

init();